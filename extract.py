try:
    from configparser import ConfigParser
except ImportError:
    from ConfigParser import ConfigParser  # ver. < 3.0
import sys

class Extract:
    def extract01(self, filename='teste.ini'):
        Dict = {}
        config = ConfigParser()
        config.read(filename)
        for section in config.sections():
            for option in config.options(section):
                print("%s.%s = %s" %(section, option, config.get(section, option)))
        print('--')
        print(config.items())
        print(config.items('section_a'))
        print(dict(config))

        print('--')
        for section in config.sections():
            print('0>', section)
            print('1>', list(config.keys()))
            print('2>', config.options(section))
            print('3>', config.items(section))
            print('4>', config.values())
            print('5>', config.__getitem__(section))
            print('6>', config.popitem())
            print(config.values().__dict__)




    def extract02(self, filename='staff.ini'):
        config = ConfigParser()
        try:
            config.read(filename)
            print(config['DEFAULT']['path'])     # -> "/path/name/"

        except KeyError:
            config['DEFAULT']['path'] = '/var/shared/'    # update
            config['DEFAULT']['default_message'] = 'Hey! help me!!'   # create
            with open(filename, 'w') as configfile:    # save
                config.write(configfile)

        except:
            raise Exception(sys.exc_info())
            print(sys.exc_info())
            pass

    def extract03(self, filename='example.ini'):
        config = ConfigParser()
        config['DEFAULT'] = {'ServerAliveInterval': '45',
                             'Compression': 'yes',
                             'CompressionLevel': '9'}
        config['bitbucket.org'] = {}
        config['bitbucket.org']['User'] = 'hg'
        config['topsecret.server.com'] = {}
        topsecret = config['topsecret.server.com']
        topsecret['Port'] = '50022'     # mutates the parser
        topsecret['ForwardX11'] = 'no'  # same here
        config['DEFAULT']['ForwardX11'] = 'yes'
        with open(filename, 'w') as configfile:
            config.write(configfile)


    def extract04(self, filename='.gitmodules'):
        DEBUG = 0
        try:
            config = ConfigParser()
            config.read(filename)
            if DEBUG: print(config.keys())

            for i in config.keys():
                print(i)

            for i in config.values():
                print(i)

            for key, item in config.items():
                print(key, item['url'], item['path'])
        except:
            print(sys.exc_info())

    def teste_method(self, filename='parserexample.ini'):
        '''
        https://wiki.python.org/moin/ConfigParserExamples
        :return:
        '''
        DEBUG = 1
        config = ConfigParser()
        try:
            open(filename)
            config.read(filename)
            if DEBUG: print(config)
            if DEBUG: print(config.sections())

            for i in config.sections():
                print('0>',i)

            sections = config.sections()

            for section in sections:
                if DEBUG: print('1>', section)

            print(config.get('Others', 'Route'))
            for i in config.items():
                print('2>',i)

            print('3>', config.options(sections[0]))

            for section in config.sections():
                for option in config.options(section):
                    print('4>',config.get(section,option))
        except FileNotFoundError:
            print('recurso não encontrado')
        except:
            print(sys.exc_info())


        pass

    def ConfigSectionMap(self, section):
        dict1 = {}
        config = ConfigParser()
        options = config.options(section)
        for option in options:
            try:
                dict1[option] = config.get(section, option)
                if dict1[option] == -1:
                    DebugPrint("skip: %s" % option)
            except:
                print("exception on %s!" % option)
                dict1[option] = None
        return dict1

    def extract05(self, filename='.gitmodules'):
        DEBUG = 0
        try:
            config = ConfigParser()
            config.read(filename)
            url, path = None, None
            for section in config.sections():
                for option in config.options(section):
                    if DEBUG: print(section, option)
                    if DEBUG: print(config.get(section, option))
                    if option == 'path':
                        path = config.get(section, option)
                    if option == 'url':
                        url = config.get(section, option)
                print("git submodule add %s %s" % (url, path))
        except:
            print(sys.exc_info())
        pass



    @classmethod
    def testes(cls):
        DEBUG0 = 0
        DEBUG1 = 1
        if DEBUG0:
            Extract().extract01()
            print('---')

            Extract().extract02()
            print('---')

            Extract().extract03()
            print('---')

            Extract().extract04()
            print('---')

            Extract().extract04('example.ini')
            print('---')

            Extract.teste_method(cls)
            Extract.extract05(cls)
        if DEBUG1:
            Extract().extract01()

    @staticmethod
    def Main():
        #Extract().extract05()
        pass

if __name__ == '__main__':
    Extract().testes()
    Extract.Main()
